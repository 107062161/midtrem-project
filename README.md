# Software Studio 2020 Spring
## Midtrem 1 ChatRoom
* Project Name : 107062161-Midtrem

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Membership Mechanism                             | 15%       | Y         |
| Host on your Firebase page                                       | 5%       | Y         |
| Database read/write                                      | 15%       | Y         |
| RWD                                   | 15%       | Y         |
| Topic Key Functions                                   | 20%       | Y         |

| **Advanced components**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Third-partyaccounts                           | 2.5%       | Y         |
| Add Chrome notification                                  | 5%       | N         |
| Use CSS animation                                       | 2.5%        | Y         |
| Deal with messages when sending html code                                         | 2.5%        | Y         |

| **Other functions**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| icon                                 | 1~10%     | Y         |


---


# 作品網址：[Firebase link](https://midtrem-3cea3.web.app/index.html) 

### How to use 

    進入index.html(你不能使用創建新房間) 按下登入/注冊，
    登入/注冊後即可創建新房間。進入房間後可邀請朋友（請使用注冊Email）.
    輸入文字之後按Send，朋友就會看到你的文字了! ^^
    
    

### Function description

*    登入頁面 <br>
![](https://i.imgur.com/cnR9RPi.jpg) <br>
主要以firebase.auth 實作
```javascript
firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
```
```javascript
var provider = new firebase.auth.GoogleAuthProvider();
firebase.auth().signInWithPopup(provider).then(function (result) {
    var token = result.credential.accessToken;
    var user = result.user;
    var userdata = {
        email: user.email,
        uid: user.uid
}
```
*    注冊頁面 <br>
![](https://i.imgur.com/xcTwuVW.jpg)<br>
主要以firebase.auth 實作
```javascript=
firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
```

*    主頁面 <br>
![](https://i.imgur.com/c4Lrt5n.jpg)<br>
主要以firebase.database() 實作（創建新房間有用到jQuery的淡入動畫)  
當`button_create_room`被按下時會在database新增一個group
```javascript
//read data
var groupid = firebase.database().ref("groupdata").push().getKey();
firebase.database().ref("userdata/" + user.uid + "/group").on("child_added", function (snapshot)    
//write data
firebase.database().ref("groupdata/" + groupid).set(groupdata);
firebase.database().ref("userdata/" + user.uid + "/group/" + num).set(groupid);
``` 

這個group的資料庫結構如下圖 箭頭為隨機產生的`token`<br>
![](https://i.imgur.com/jqOdDPM.jpg)

*    進入房間按鈕 <br>
這個我主要利用hash屬性把`token`打進去再換頁到`room.html`
利用這個可以在進入房間後知道你的房間`token`是什麼。
```
https://midtrem-3cea3.web.app/room.html#[token]
```

*    房間頁面 <br>
![](https://i.imgur.com/vKhQ6B7.jpg)
主要利用database write/read的特點

```javascript
//...
//write message to database
var message = document.getElementById("message");
if (message.value != "") {
    var content = {
        email: user.email,
        value: message.value,
    }
    firebase.database().ref("groupdata/" + token + "/message").push().set(content);
    message.value = ""
}
else {
    alert("你必須要輸入點東西啊!")
}
//...
//read database message
firebase.database().ref("groupdata/" + token + "/message").on("child_added", function (snapshot) {
    var html = "";
    html += "<h5>"
    html += snapshot.val().email + ": " + dealwithwords(snapshot.val().value);
    html += "</h5>"

    document.getElementById("message_send").innerHTML += html;
});
``` 
*    處理特別符號 <br>
```javascript
function dealwithwords(txt) {
    return txt.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
}
```
*    邀請朋友 <br>
利用database搜尋有沒有這個Email跟判定這個Email在不在房間內
```javascript
var invite_name = document.getElementById("invite-name");
firebase.database().ref().child('userdata').orderByChild('email').equalTo(invite_name.value).on('child_added', function (snap) {
uid = snap.key;
firebase.database().ref("groupdata/" + token + "/member").once("value", function (snape) {
    firebase.database().ref("groupdata/" + token + "/owner").once("value", function (snape1) {
        firebase.database().ref("userdata/" + uid + "/group").on("child_added", function (snapshot) {
            num++;
        });
        if (snape.val() != invite_name.value && snape1.val() != invite_name.value) {
            firebase.database().ref("groupdata/" + token).update({ member: invite_name.value });
            firebase.database().ref("userdata/" + uid + "/group/" + num).set(token);
        }
        else {
            alert("你邀請的朋友已經在這個房間了");
        }
    });
});
```

  
*    其它 <br>
我有改網頁的icon ![](https://i.imgur.com/qw9Bj9e.png)
