function init() {
    var btn_id = document.getElementById("status");
    var btn_out = document.getElementById("log-out");
    var btn_create = document.getElementById("create-room");
    var num = 0;
    var i = 0;
    var invite_bool = false;

    setTimeout(function () {
        document.getElementById('create-room').disabled = true;
    }, 0);

    setTimeout(function () {
        document.getElementById('create-room').disabled = false;
        console.log("OK");
    }, 2566);

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            btn_id.innerHTML = user.email;
            btn_id.href = "index.html";
            btn_out.innerHTML = "Logout";

            btn_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('You are logout now!')
                        window.location.href = 'index.html';
                    })
                    .catch(function (e) {
                        alert('Fail to Logout!')
                    });
            });
            //2020
            firebase.database().ref("userdata/" + user.uid + "/group").on("child_added", function (snapshot) {
                var token = "";
                firebase.database().ref("userdata/" + user.uid + "/group/" + i).once('value', e => {
                    token = e.val();
                });
                i++;
                var html = "";
                html += "<button id=\"" + i + "\" class=\"button2\" onclick=\"location.href=\'room.html#" + token + "\'\">";
                html += "ROOM " + i;
                html += "</button>"
                console.log(html);
                document.getElementById("new-room").innerHTML = html + document.getElementById("new-room").innerHTML;
                document.getElementById("no-room").innerHTML = "";
            });

            btn_create.addEventListener('click', function () {
                var groupid = firebase.database().ref("groupdata").push().getKey();
                //read
                num = 0;
                firebase.database().ref("userdata/" + user.uid + "/group").on("child_added", function (snapshot) {
                    num++;
                });
                var groupdata = {
                    id: groupid,
                    owner: user.email,
                    member: ""
                }
                //write
                firebase.database().ref("groupdata/" + groupid).set(groupdata);
                firebase.database().ref("userdata/" + user.uid + "/group/" + num).set(groupid);

            });


        }
        else {
            btn_id.innerHTML = "登入";
            btn_out.href = "signup.html";
            btn_out.innerHTML = "注冊";
        }
    });

}

window.onload = function () {
    init();
}