var uid;
function init() {
    var btn_id = document.getElementById("status");
    var btn_out = document.getElementById("log-out");
    var btn_submit = document.getElementById("submit");
    var btn_invite = document.getElementById("invite");
    var token = location.hash.replace(/#/g, '');
    var num = 0;

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            btn_id.innerHTML = user.email;
            btn_id.href = "index.html";
            btn_out.innerHTML = "Logout";
            btn_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('You are logout now!')
                        window.location.href = 'index.html';
                    })
                    .catch(function (e) {
                        alert('Fail to logout!')
                    });
            });
            
            //invitaion
            btn_invite.addEventListener('click', function () {
                num = 0;
                var invite_name = document.getElementById("invite-name");
                firebase.database().ref().child('userdata').orderByChild('email').equalTo(invite_name.value).on('child_added', function (snap) {
                    uid = snap.key;
                    firebase.database().ref("groupdata/" + token + "/member").once("value", function (snape) {
                        firebase.database().ref("groupdata/" + token + "/owner").once("value", function (snape1) {
                            firebase.database().ref("userdata/" + uid + "/group").on("child_added", function (snapshot) {
                                num++;
                            });
                            if (snape.val() != invite_name.value && snape1.val() != invite_name.value) {
                                firebase.database().ref("groupdata/" + token).update({ member: invite_name.value });
                                firebase.database().ref("userdata/" + uid + "/group/" + num).set(token);
                            }
                            else {
                                alert("你邀請的朋友已經在這個房間了");
                            }
                        });
                    });

                });

            });

            //submit message to database
            btn_submit.addEventListener('click', function () {
                //get message
                var message = document.getElementById("message");
                if (message.value != "") {
                    var content = {
                        email: user.email,
                        value: message.value,
                    }
                    firebase.database().ref("groupdata/" + token + "/message").push().set(content);
                    message.value = ""
                }
                else {
                    alert("你必須要輸入點東西啊!")
                }
            });

            firebase.database().ref("groupdata/" + token + "/message").on("child_added", function (snapshot) {

                var html = "";
                html += "<h5>"
                html += snapshot.val().email + ": " + dealwithwords(snapshot.val().value);
                html += "</h5>"

                document.getElementById("message_send").innerHTML += html;
            });

        }
        else {
            btn_id.innerHTML = "登入";
            btn_out.href = "signup.html";
            btn_out.innerHTML = "注冊";
        }
    });

}

function dealwithwords(txt) {
    return txt.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
}

window.onload = function () {
    //todo - Web notification
    init();
}