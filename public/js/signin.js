function init() {
    var txtEmail = document.getElementById("username");
    var txtPassword = document.getElementById("password");
    var btn_login = document.getElementById("login");
    var btn_google = document.getElementById("google");

    btn_login.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
            .then(function (result) {
                alert("登入成功");
                window.location.href = 'index.html';
            })
            .catch(function (error) {
                alert("登入失敗");
            });

        txtEmail.value = "";
        txtPassword.value = "";
    });

    btn_google.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            var userdata = {
                email: user.email,
                uid: user.uid
            }
            firebase.database().ref("userdata/" + user.uid).update(userdata);
            var ref = firebase.database().ref(); 
            ref.once('value')
                .then(function (result) {
                    alert("登入成功")
                    window.location.href = 'index.html';
                })
                .catch(function (err) {
                    console.log('Error', err.code);
                });
        }).catch(function (error) {
            console.log(error);
        });
    });
}

window.onload = function () {
    init();
}