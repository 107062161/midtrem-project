function init() {
    var back = document.getElementById("go-back");
    var signup = document.getElementById("signup");
    var txtEmail = document.getElementById("email");
    var txtPassword = document.getElementById("password");
    var txtPassword_Con = document.getElementById("password-con");

    back.addEventListener('click', function () {
        window.location.href = 'signin.html';
    });

    signup.addEventListener('click', function () {
        if (txtPassword.value == txtPassword_Con.value) { 
            firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
                .then(function (firebaseUser) {
                    var user = firebase.auth().currentUser;
                    var userdata = {
                        email: txtEmail.value,
                        uid: user.uid,
                        group: ""
                    }
                    firebase.database().ref("userdata/" + user.uid).set(userdata);
                    //wait database responce
                    var ref = firebase.database().ref(); 
                    ref.once('value')
                        .then(function (result) {
                            alert("成功注冊");
                            window.location.href = 'index.html';
                        })
                        .catch(function (err) {
                            console.log('Error', err.code);
                        });
                })
                .catch(function (error) {
                    alert(error);
                });
        }
        else {
            alert("密碼不一致，請重新輸入");
        }
        txtPassword.value = "";
        txtPassword_Con.value = "";
    });
}

window.onload = function () {
    init();
}